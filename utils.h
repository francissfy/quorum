//
// Created by Shen Feiyu on 2022/3/9.
//

#ifndef QUORUM_UTILS_H_
#define QUORUM_UTILS_H_

#include <vector>
#include <string>
#include <iostream>
#include "idl_types.h"

using HostT_ = std::pair<std::string, int>;
using HostListT_ = std::vector<HostT_>;

typedef struct {
    std::string key;
    std::string op;
    std::string ret;
} TestCommandT_;

std::ostream& operator<<(std::ostream& stream, const HostT_& host);

HostListT_ parse_member_ips(const std::string& ip_str);

HostT_ parse_addr(const std::string& addr);

const char* get_error_msg(quorum::ErrCode::type code);

#endif //QUORUM_UTILS_H_
