//
// Created by Shen Feiyu on 2022/3/9.
//

#include <vector>
#include <gflags/gflags.h>
#include <glog/logging.h>

#include <thrift/concurrency/ThreadManager.h>
#include <thrift/concurrency/ThreadFactory.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TThreadPoolServer.h>
#include <thrift/server/TThreadedServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

#include "QuorumHandler.h"

using namespace ::apache::thrift;
using namespace ::apache::thrift::concurrency;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

DEFINE_int32(port, 9901, "server_port");

DEFINE_string(master_host, "", "master host");


int main(int argc, char** argv) {
    // google::InitGoogleLogging(argv[0]);
    gflags::ParseCommandLineFlags(&argc, &argv, true);

    HostT_ self_host = std::make_pair("localhost", FLAGS_port);

    std::shared_ptr<QuorumServiceHandler> handler;
    if (FLAGS_master_host.empty()) {
        handler = std::make_shared<QuorumServiceHandler>(self_host);
    } else {
        HostT_ master_host = parse_addr(FLAGS_master_host);
        handler = std::make_shared<QuorumServiceHandler>(self_host, master_host);
    }
    std::shared_ptr<TProcessor> processor(new QuorumServiceProcessor(handler));

    const int worker_count = 3;
    std::shared_ptr<ThreadManager> thread_manager = ThreadManager::newSimpleThreadManager(worker_count);
    thread_manager->threadFactory(std::make_shared<ThreadFactory>());
    thread_manager->start();

    std::shared_ptr<TServerTransport> serverTransport(new TServerSocket(FLAGS_port));
    std::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
    std::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

    TThreadPoolServer server(processor, serverTransport, transportFactory, protocolFactory, thread_manager);
    server.serve();

    return 0;
}