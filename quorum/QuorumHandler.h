//
// Created by Shen Feiyu on 2022/3/9.
//

#ifndef QUORUM_QUORUMHANDLER_H_
#define QUORUM_QUORUMHANDLER_H_

#include <vector>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <map>
#include <glog/logging.h>
/* thrift library */
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TTransportUtils.h>

#include "QuorumService.h"
#include "idl_types.h"
#include "utils.h"

using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

using namespace quorum;

class QuorumServiceHandler: public QuorumServiceIf {
public:
    /* init as a master server */
    QuorumServiceHandler(const HostT_& self_addr): is_leader_(true), self_addr_(self_addr) {
        LOG(INFO) << "Initializing MASTER server: " << self_addr_;
    }

    /* init as a slave server */
    QuorumServiceHandler(const HostT_& self_addr, const HostT_& master_addr): is_leader_(false), self_addr_(self_addr) {
        /* register self to master server */
        std::shared_ptr<TTransport> socket(new TSocket(master_addr.first, master_addr.second));
        std::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
        std::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
        auto client = std::make_unique<QuorumServiceClient>(protocol);
        BaseResp base_resp;
        auto self_host = Host();
        self_host.ip = self_addr_.first;
        self_host.port = self_addr_.second;
        try {
            transport->open();
            client->RegisterSlave(base_resp, self_host);
            transport->close();
            LOG(INFO) << "Success register to MASTER: " << master_addr;
        } catch (TException& tx) {
            LOG(ERROR) << "Error register to MASTER server: " << master_addr;
            exit(-1);
        }
        member_addrs_.push_back(master_addr);
        LOG(INFO) << "Initializing SLAVE server: " << self_addr_ << "; MASTER: " << master_addr;
    }

    void RegisterSlave(BaseResp &_return, const Host &host) override {
        _return.code = ErrCode::OK;
        if (!is_leader_) {
            _return.code = ErrCode::ERR_REGISTER_SLAVE;
            LOG(ERROR) << get_error_msg(_return.code);
            return;
        }
        /* check duplication */
        HostT_ slave_host = std::make_pair(host.ip, host.port);
        auto iter = std::find(member_addrs_.begin(), member_addrs_.end(), slave_host);
        if (iter != member_addrs_.end()) {
            _return.code = ErrCode::ERR_EXISTING_ADDRESS;
            LOG(ERROR) << get_error_msg(_return.code);
            return;
        }
        member_addrs_.push_back(slave_host);
        LOG(INFO) << "SLAVE server registered! Slave host: "
                  << slave_host.first << ":" << slave_host.second;
    }

    /* sync self.lockMap */
    void SyncLockMap(BaseResp &_return, const SyncReq &req) override {
        {   /* own unique write lock */
            std::unique_lock lock(sync_lock_);
            LOG(INFO) << "Begin sync lockMap from master";
            lock_map_ = req.lockMap;
        }
        _return.code = ErrCode::OK;
        LOG(INFO) << "Finish sync lockMap from master";
    }

    void DistributedLock(ClientResp &_return, const ClientReq &req) override {
        _return.base.code = ErrCode::OK;
        if (req.op == LockOps::QUIRE) {
            {   /* own shared read lock */
                std::shared_lock lock(sync_lock_);
                auto iter = lock_map_.find(req.lockKey);
                if (iter == lock_map_.end()) {
                    _return.base.code = ErrCode::ERR_NOT_FOUND_KEY;
                } else {
                    _return.ownerID = iter->second;
                }
            }
            return;
        } else {
            if (is_leader_) {
                SyncReq sync_req;
                /* process in master */
                {   /* own unique write lock */
                    std::unique_lock lock(sync_lock_);
                    auto iter = lock_map_.find(req.lockKey);
                    if (req.op == LockOps::LOCK) {
                        if (iter == lock_map_.end()) {
                            lock_map_.insert(std::make_pair(req.lockKey, req.clientID));
                        } else {
                            _return.base.code = ErrCode::ERR_KEY_OWNED;
                        }
                    } else {
                        /* UNLOCK */
                        if (iter == lock_map_.end()) {
                            _return.base.code = ErrCode::ERR_NOT_FOUND_KEY;
                        } else {
                            lock_map_.erase(iter);
                        }
                    }
                    sync_req.lockMap = lock_map_;
                }
                /* if operation failed, do not update slaves */
                if (_return.base.code != ErrCode::OK) {
                    return;
                }
                /* request members to sync */
                for (const HostT_& host: member_addrs_) {
                    std::shared_ptr<TTransport> socket(new TSocket(host.first, host.second));
                    std::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
                    std::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
                    auto client = std::make_unique<QuorumServiceClient>(protocol);
                    try {
                        transport->open();
                        client->SyncLockMap(_return.base, sync_req);
                        transport->close();
                        LOG(INFO) << "Sync to slave: " << host << " SUCCESS";
                    } catch (TException& tx) {
                        LOG(ERROR) << "Sync to slave: " << host << " ERROR: " << tx.what();
                        _return.base.code = ErrCode::ERR_SYNC_KEY;
                    }
                }
            } else {
                /* forward request to master */
                const HostT_ & master_host = member_addrs_[0];
                std::shared_ptr<TTransport> socket(new TSocket(master_host.first, master_host.second));
                std::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
                std::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
                auto client = std::make_unique<QuorumServiceClient>(protocol);
                try {
                    LOG(INFO) << "Forward to master: " << master_host;
                    transport->open();
                    client->DistributedLock(_return, req);
                    transport->close();
                } catch (TException& tx) {
                    LOG(INFO) << "Forward to master: " << master_host << " ERROR: " << tx.what();
                    _return.base.code = ErrCode::ERR_FORWARD_REQ;
                }
            }
        }
    }

private:
    HostT_ self_addr_;
    HostListT_ member_addrs_;
    bool is_leader_;
    std::map<std::string, std::string> lock_map_;
    std::shared_mutex sync_lock_;
};


#endif //QUORUM_QUORUMHANDLER_H_
