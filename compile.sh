#!/bin/bash

if thrift --version; then
:
else
  echo "you need thrift to compile RPC interface and to link against"
  exit
fi

function ThriftGen() {
  cd idl || exit
  # verify MD5
  if [ -f MD5 ] && [ "$(cat MD5)" = "$(md5 idl.thrift)" ]; then
    echo "idl.thrift not changed, skip generation"
  else
    echo "Compiling RPC interface..."
    thrift -r --gen cpp idl.thrift || exit
    md5 idl.thrift > MD5
  fi
  cd ..
}

function CompileQuorum() {
  echo "Compiling Quorum Server..."
  [[ -d build ]] || mkdir build
  cd build && cmake -G Ninja Quorum .. || exit
  cmake --build . --target Quorum || exit
  cd ..
}

function CompileQuorumTest() {
  echo "Compiling Quorum GTest..."
  [[ -d build ]] || mkdir build
  cd build && cmake -G Ninja ClientGtest .. || exit
  cmake --build . --target ClientGtest || exit
  cd ..
}

# compile RPC file
ThriftGen

# compile server
CompileQuorum

# compile test client
CompileQuorumTest



