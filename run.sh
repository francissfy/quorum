#!/bin/bash
set -e
set -o pipefail
trap 'pkill -P $$' SIGINT SIGTERM

master_port=9901
slave_ports=(9902 9903 9904)

# compile server and test client
# ./compile.sh

pids=()
# launch master server
echo "Launch master with port: ${master_port}"
(
    ./build/Quorum --port ${master_port}
) &
pids+=($!)
sleep 0.2

for sp in "${slave_ports[@]}";do
    echo "Launch slave with port: ${sp}"
    (
        ./build/Quorum --port ${sp} --master_host "localhost:${master_port}"
        sleep 0.2
    )&
    pids+=($!)
    sleep 0.2
done

