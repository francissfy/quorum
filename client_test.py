import sys
sys.path.append("idl/gen-py/")

from thrift import Thrift
from thrift.transport import TSocket, TTransport
from thrift.protocol import TBinaryProtocol
from idl.ttypes import ClientReq, ClientResp, LockOps
from idl.QuorumService import Client as QuorumClient

def main():
    transport = TSocket.TSocket("localhost", 9901)
    transport = TTransport.TBufferedTransport(transport)
    protocol = TBinaryProtocol.TBinaryProtocol(transport)

    client = QuorumClient(protocol)

    req = ClientReq(op=LockOps.LOCK, lockKey="l1", clientID="c1")

    transport.open()
    resp:ClientResp = client.DistributedLock(req)
    transport.close()

    print(resp.base.status, resp.base.msg, resp.ownerID)

if __name__ == "__main__":
    main()
