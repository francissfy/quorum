//
// Created by Shen Feiyu on 2022/3/9.
//

#include "utils.h"

std::ostream& operator<<(std::ostream& stream, const HostT_& host) {
    stream << host.first << ":" << host.second;
    return stream;
}

HostListT_ parse_member_ips(const std::string& ip_str) {
    size_t sidx = 0;
    HostListT_ hosts;
    std::string ip;
    for (size_t i=0; i<ip_str.length(); ++i) {
        if (ip_str[i] == ':') {
            ip = ip_str.substr(sidx, i-sidx);
            sidx = i+1;
        } else if (ip_str[i] == ',') {
            std::string port = ip_str.substr(sidx, i-sidx);
            sidx = i+1;
            hosts.push_back(std::make_pair(ip, std::stoi(port)));
        }
    }
    std::string port = ip_str.substr(sidx, ip_str.length()-sidx);
    hosts.push_back(std::make_pair(ip, std::stoi(port)));
    return hosts;
}

HostT_ parse_addr(const std::string& addr) {
    size_t col_idx = addr.find(':');
    std::string ip = addr.substr(0, col_idx);
    std::string port = addr.substr(col_idx+1, addr.length()-col_idx);
    return std::make_pair(ip, std::stoi(port));
}

const char* get_error_msg(quorum::ErrCode::type code) {
    using namespace quorum;
    if (code == ErrCode::OK) {
        return "Success";
    } else if (code == ErrCode::ERR_REGISTER_SLAVE) {
        return "Error registering slave server";
    } else if (code == ErrCode::ERR_NOT_FOUND_KEY) {
        return "Error key not found";
    } else if (code == ErrCode::ERR_EXISTING_ADDRESS) {
        return "Error address already existed";
    } else if (code == ErrCode::ERR_SYNC_KEY) {
        return "Error syncing key map";
    } else if (code == ErrCode::ERR_FORWARD_REQ) {
        return "Error forwarding request to master server";
    } else {
        return "Unknown error";
    }
}
