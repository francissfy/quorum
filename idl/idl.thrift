namespace cpp quorum

enum ErrCode {
    OK;
    ERR_REGISTER_SLAVE;
    ERR_EXISTING_ADDRESS;
    ERR_NOT_FOUND_KEY;
    ERR_EXISTING_KEY;
    ERR_SYNC_KEY;
    ERR_FORWARD_REQ;
    ERR_KEY_OWNED;
}

struct BaseResp {
    1: ErrCode code;
    2: string msg;
}

enum LockOps {
    LOCK;
    UNLOCK;
    QUIRE;
}

struct Host {
    1: string ip;
    2: i32 port;
}

struct ClientReq {
    1: required LockOps op;
    2: required string lockKey;
    3: required string clientID;
}

struct ClientResp {
    1: required BaseResp base;
    2: required string ownerID;
}

struct SyncReq {
    1: required map<string, string> lockMap;
}

service QuorumService {
    ClientResp DistributedLock(1: ClientReq req);
    BaseResp SyncLockMap(1: SyncReq req);
    BaseResp RegisterSlave(1: Host host);
}
