#include "idl_types.h"
#include "QuorumService.h"
#include "utils.h"
#include "gtest/gtest.h"
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include <fstream>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TBufferTransports.h>
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

#define GTEST_INFO(msg, ...) printf("\033[0;32m[   INFO   ] \033[0;33m" msg "\033[0;m\n", ## __VA_ARGS__);

void read_test_json(std::vector<TestCommandT_>& cmd_ret, std::vector<HostT_>& server_ret, const char* json_file) {
    using namespace rapidjson;
    std::vector<TestCommandT_> ret;
    Document doc;
    /* read json file */
    std::ifstream ifs(json_file);
    IStreamWrapper isw(ifs);
    if (doc.ParseStream(isw).HasParseError())
        exit(-1);
    /* read servers */
    const Value& servers = doc["servers"];
    for (SizeType i=0; i<servers.Size(); ++i) {
        const std::string& addr = servers[i].GetString();
        server_ret.emplace_back(parse_addr(addr));
    }
    /* read commands */
    const Value& cmds = doc["commands"];
    for (SizeType i=0; i<cmds.Size(); ++i) {
        const Value& elem = cmds[i];
        TestCommandT_ cmd{
                .key = elem[0].GetString(),
                .op = elem[1].GetString(),
                .ret = elem[2].GetString()
        };
        cmd_ret.push_back(cmd);
    }
}


class QUORUM_GTEST: public ::testing::TestWithParam<const char*> {
protected:
    std::vector<TestCommandT_> test_commands_;
    std::vector<HostT_> test_hosts_;
    std::vector<std::unique_ptr<quorum::QuorumServiceClient>> test_clients_;
    std::vector<std::shared_ptr<TTransport>> test_client_transports_;

public:
    void SetUp() override {
        const char* json_file = GetParam();
        read_test_json(test_commands_, test_hosts_, json_file);

        for (const HostT_& host: test_hosts_) {
            std::shared_ptr<TTransport> socket(new TSocket(host.first, host.second));
            std::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
            std::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
            auto client = std::make_unique<quorum::QuorumServiceClient>(protocol);
            test_clients_.push_back(std::move(client));
            test_client_transports_.push_back(std::move(transport));
        }
    }

    void TearDown() override {

    }
};


quorum::LockOps::type convert_to_req_op(const std::string& op) {
    if (op == "LOCK")
        return quorum::LockOps::LOCK;
    else if (op == "UNLOCK")
        return quorum::LockOps::UNLOCK;
    return quorum::LockOps::QUIRE;
}


TEST_P(QUORUM_GTEST, TEST_WITH_JSON) {
    for (const TestCommandT_& cmd: test_commands_) {
        quorum::ClientReq req;
        quorum::ClientResp resp;

        int idx = rand()%test_hosts_.size();

        req.lockKey = cmd.key;
        req.op = convert_to_req_op(cmd.op);
        req.clientID = std::to_string(test_hosts_[idx].second);

        try {
            test_client_transports_[idx]->open();
            test_clients_[idx]->DistributedLock(resp, req);
            test_client_transports_[idx]->close();
        } catch (TException& tx) {
            ASSERT_TRUE(false) << "Send request fail: " << tx.what();
        }

        GTEST_INFO("PORT:%d\tKEY:%s\t OP:%s\t RET:%s", test_hosts_[idx].second, cmd.key.c_str(), cmd.op.c_str(), cmd.ret.c_str());
        EXPECT_EQ(resp.base.code==quorum::ErrCode::OK, cmd.ret == "OK") << get_error_msg(resp.base.code);
    }
}


INSTANTIATE_TEST_SUITE_P(QUORUM, QUORUM_GTEST, ::testing::Values("test_v1.json"));


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
