include(ExternalProject)

ExternalProject_Add(
    gflags
    URL https://github.com/gflags/gflags/archive/refs/tags/v2.2.2.tar.gz
    URL_MD5 1a865b93bacfa963201af3f75b7bd64c
    PREFIX ${PROJECT_SOURCE_DIR}/thirdparty/gflags
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${PROJECT_SOURCE_DIR}/thirdparty/gflags/build -DBUILD_STATIC_LIBS=ON
)

ExternalProject_Add(
    glog
    URL https://github.com/google/glog/archive/refs/tags/v0.6.0.tar.gz
    URL_MD5 c98a6068bc9b8ad9cebaca625ca73aa2
    PREFIX ${PROJECT_SOURCE_DIR}/thirdparty/glog
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${PROJECT_SOURCE_DIR}/thirdparty/glog/build -DBUILD_SHARED_LIBS=OFF
)

ExternalProject_Add(
    gtest
    URL https://github.com/google/googletest/archive/refs/tags/release-1.11.0.tar.gz
    URL_MD5 b4240170c09c76eeb8327631f1ae6ebe
    PREFIX ${PROJECT_SOURCE_DIR}/thirdparty/gtest
    CMAKE_ARGS -DINSTALL_GTEST=OFF -DBUILD_GMOCK=OFF
    INSTALL_COMMAND mkdir -p ../../build &&
                    cp -r lib  ../../build &&
                    cp -r ../gtest/googletest/include ../../build
)

ExternalProject_Add(
    rapidjson
    URL https://github.com/Tencent/rapidjson/archive/refs/tags/v1.1.0.tar.gz
    URL_MD5 0e5c3c7feea6b55b94f7597f6491ffe8
    PREFIX ${PROJECT_SOURCE_DIR}/thirdparty/rapidjson
    CMAKE_ARGS -DCMAKE_CXX_FLAGS="-w" -DCMAKE_INSTALL_PREFIX:PATH=${PROJECT_SOURCE_DIR}/thirdparty/rapidjson/build
)
